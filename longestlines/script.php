<?php

$data = file($argv[1], FILE_IGNORE_NEW_LINES);

$count = array_shift($data);

usort($data, function($a,$b) {
    return strlen($b)-strlen($a);
});

$data = array_slice($data, 0, $count);

foreach ($data as $line) {
    echo($line . PHP_EOL);
}

exit;