<?php

$data = file($argv[1], FILE_IGNORE_NEW_LINES);

foreach ($data as $test) {
    list($x, $y, $n) = explode(' ', $test);

    $output = '';
    for ($i = 1; $i <= $n; $i++) {
        $fizzbuzz = ($i % $x ? '' : 'F') . ($i % $y ? '' : 'B');
        $output .= (($fizzbuzz) ?: $i) . ' ';
    }
    echo(trim($output) . PHP_EOL);
}

exit;
