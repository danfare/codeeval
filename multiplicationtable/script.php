<?php

$y = $x = range(1,12);


foreach ($y as $yIdx => $vert) {
    $row = array();
    foreach ($x as $xIdx => $horiz) {
        $format = ($xIdx === 0) ? '%s' : '%4s';
        $row[] = sprintf($format, $vert * $horiz);
    }

    echo implode('', $row) . PHP_EOL;
}

exit;